Registro de Ponto

DOCUMENTAÇÃO

requisição e resposta no formato JSON


************************************************ Usuario *************************************************

GET  /usuarios - (Consulta todos os usuarios cadastrados no sistema)

Envio:
/usuarios

Resposta:
[
    {
        "nomeCompleto": "Fernando Jacyntho",
        "cpf": 32036550800,
        "email": "fernando@fernando.com",
        "dataCadastro": "2020-07-03"
    },
    ...
 
    
GET  /usuarios/usuario/{idUsuario} - (Consulta usuario cadastrado no sistema através do id)

Envio:
/usuarios/usuario/1

Resposta:
[
    {
        "nomeCompleto": "Fernando Jacyntho",
        "cpf": 32036550800,
        "email": "fernando@fernando.com",
        "dataCadastro": "2020-07-03"
    }
]


POST /usuarios/cadUsuario - (Cadastrar usuario no sistema)
 
 Envio:
 /usuarios/cadUsuario
 
 + JSON
 { "nomeCompleto":"Fernando Jacyntho", "email":"fernando@fernando.com", "cpf":32036550800, "dataCadastro":"2020-07-03" }
 
 Resposta:
 {
     "data": "OK",
     "errors": []
 }
 
 
PUT  /usuarios/usuario/{idUsuario} - (Alterar dados do usuario através do id)

Envio:	
/usuarios/usuario/1
+ JSON
{ "nomeCompleto":"Fernando Martini", "email":"teste@fernando.com.br", "cpf":32036550800, "dataCadastro":"12/12/2020"  }
--> Data nao será alterada

Resposta:
{
    "data": "OK",
    "errors": []
}


************************************************ Ponto *************************************************

POST  /ponto/batida - (Cadastrar o ponto no sistema)
	
Envio:
/ponto/batida
+JSON
{  "idUsuario": 1,  "tipoBatida": 1  }
--> tipoBatida (1 = Entrada e 2 = Saída)

Resposta:
{
    "data": "OK",
    "errors": []
}


GET /ponto/batida/{idUsuario} - (Consultar detalhes dos registros de ponto de um usuario através de seu id)

Envio:
/ponto/batida/1

Resposta:

{
    "nomeUsuario": "Fernando Jacyntho",
    "batidas": [
        {
            "tipoBatida": "Entrada",
            "dataHoraBatida": "03-07-2020 20:50:00"
        },
        {
            "tipoBatida": "Saída",
            "dataHoraBatida": "03-07-2020 21:50:42"
        }
    ],
    "horasTrabalhadas": "01:00:42 horas"
}

-->	O campo "horasTrabalhadas" é calculado através das sequencias de entrada/saída, se nao for nessa sequencia, o mesmo fica zerado.
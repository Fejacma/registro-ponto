package br.com.registrodeponto.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.registrodeponto.models.DTOs.UsuarioDTO;
import br.com.registrodeponto.response.Response;
import br.com.registrodeponto.service.UsuarioService;
@RestController
@RequestMapping("/usuarios")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;

	
	@PostMapping("/cadUsuario")
	@ResponseBody
	public ResponseEntity<Response<String>> createUsuario(@RequestBody @Valid UsuarioDTO dadosUsuario, BindingResult result) {
		Response<String> response = new Response<String>();
		dadosUsuario.setDataCadastro(LocalDate.now());
		if (result.hasErrors()) {
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}else if(usuarioService.create(dadosUsuario)!= null) {
			response.setData("OK");
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}else {
			response.setData("Erro");
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
		}
		
	}
	
	
	@GetMapping("/usuario/{idUsuario}")
	@ResponseBody
	public UsuarioDTO getUsuarios(@PathVariable(value = "idUsuario") long idUsuario) {
		return usuarioService.getUsuario(idUsuario);
	}
	@GetMapping
	@ResponseBody
	public List<UsuarioDTO> getUsuarios() {
		return usuarioService.listarUsuarios();
	}
	@PutMapping("/usuario/{idUsuario}")
	@ResponseBody
	public ResponseEntity<Response<String>> updateUsuario(@PathVariable(value = "idUsuario") long idUsuario,@RequestBody @Valid UsuarioDTO dadosUsuario){
		Response<String> response = new Response<String>();
		if(usuarioService.update(idUsuario,dadosUsuario) == null) {
			response.setData("Erro");
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
		}else {
			response.setData("OK");
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}
		
	}

}

package br.com.registrodeponto.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.registrodeponto.models.DTOs.RegistroDetalhadoDTO;
import br.com.registrodeponto.models.DTOs.RegistroDePontoDTO;
import br.com.registrodeponto.models.Usuario;
import br.com.registrodeponto.models.RegistroDetalhado;
import br.com.registrodeponto.models.RegistroDePonto;
import br.com.registrodeponto.repositories.UsuarioRepository;
import br.com.registrodeponto.repositories.RegistroDePontoRepository;

@Service
public class RegistroDePontoService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private RegistroDePontoRepository registroDePontoRepository;

	public RegistroDePonto create(RegistroDePontoDTO dadosBatidaDto) {

		Optional<Usuario> opUsuario = usuarioRepository.findById(dadosBatidaDto.getIdUsuario());
		if (opUsuario.isPresent()) {
			RegistroDePonto dadosBatida = new RegistroDePonto();
			dadosBatida.setTipoBatida(dadosBatidaDto.getTipoBatida());
			dadosBatida.setResponsavel(opUsuario.get());
			dadosBatida.setDataHoraBatida(LocalDateTime.now());

			return registroDePontoRepository.save(dadosBatida);
		}
		return null;
	}

	public RegistroDetalhadoDTO getBatidasByUsuario(Long idUsuario) {
		Usuario dadosUsuario = new Usuario();
		dadosUsuario.setId(idUsuario);
		List<RegistroDetalhado> listaDetalhes = new ArrayList<RegistroDetalhado>();
		List<RegistroDePonto> dadosBatidas = registroDePontoRepository.findAllByResponsavel(dadosUsuario);
		if(dadosBatidas.isEmpty()) {
			return null;
		}
		dadosBatidas.forEach(batida -> listaDetalhes.add(
				new RegistroDetalhado(batida.getTipoBatida() == 1 ? "Entrada" : "Saída", batida.getDataHoraBatida())));

		RegistroDetalhadoDTO dadosBatidaDetalhada = new RegistroDetalhadoDTO();
		dadosBatidaDetalhada.setNomeUsuario(dadosBatidas.get(1).getResponsavel().getNomeCompleto());
		dadosBatidaDetalhada.setBatidas(listaDetalhes);
		dadosBatidaDetalhada.setHorasTrabalhadas(getHorasTrabalhadas(listaDetalhes));
		return dadosBatidaDetalhada;
	}

	public String getHorasTrabalhadas(List<RegistroDetalhado> listaDetalhes) {

		long millis = 0;
		for (int i = 0; i < listaDetalhes.size(); i++) {
			if (listaDetalhes.get(i).getTipoBatida().equalsIgnoreCase("entrada")) {
				if (listaDetalhes.get(i + 1).getTipoBatida().equalsIgnoreCase("saída")) {
					Duration dur = Duration.between(listaDetalhes.get(i).dataHoraBatidaLocalDateTime(),
							listaDetalhes.get(i + 1).dataHoraBatidaLocalDateTime());
					millis = millis + dur.toMillis();
				}
			}
		}
		return String.format("%02d:%02d:%02d horas", TimeUnit.MILLISECONDS.toHours(millis),
				TimeUnit.MILLISECONDS.toMinutes(millis)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
				TimeUnit.MILLISECONDS.toSeconds(millis)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
	}

}

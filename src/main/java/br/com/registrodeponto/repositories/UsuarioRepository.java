package br.com.registrodeponto.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.registrodeponto.models.Usuario;



public  interface UsuarioRepository extends JpaRepository<Usuario, Long>{

}

package br.com.registrodeponto.models.DTOs;

import javax.validation.constraints.NotNull;

public class RegistroDePontoDTO {

	@NotNull(message = "idUsuario é obrigatório")
	private Long idUsuario;
	@NotNull(message = "tipoBatida é obrigatório")
	private Integer tipoBatida;

	public RegistroDePontoDTO() {
	};

	public RegistroDePontoDTO(Long idUsuario, Integer tipoBatida) {
		super();
		this.idUsuario = idUsuario;
		this.tipoBatida = tipoBatida;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Integer getTipoBatida() {
		return tipoBatida;
	}

	public void setTipoBatida(Integer tipoBatida) {
		this.tipoBatida = tipoBatida;
	}

}

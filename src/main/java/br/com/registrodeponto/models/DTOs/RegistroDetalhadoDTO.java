package br.com.registrodeponto.models.DTOs;

import java.util.List;

import br.com.registrodeponto.models.RegistroDetalhado;

public class RegistroDetalhadoDTO {
	
	private String nomeUsuario;
	
	private List<RegistroDetalhado> batidas;
	
	private String horasTrabalhadas;

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public List<RegistroDetalhado> getBatidas() {
		return batidas;
	}

	public void setBatidas(List<RegistroDetalhado> batidas) {
		this.batidas = batidas;
	}

	public String getHorasTrabalhadas() {
		return horasTrabalhadas;
	}

	public void setHorasTrabalhadas(String horasTrabalhadas) {
		this.horasTrabalhadas = horasTrabalhadas;
	}
	

}

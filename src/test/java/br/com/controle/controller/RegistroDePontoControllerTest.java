package br.com.controle.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import br.com.registrodeponto.controller.RegistroDePontoController;
import br.com.registrodeponto.models.DTOs.RegistroDetalhadoDTO;
import br.com.registrodeponto.models.DTOs.RegistroDePontoDTO;
import br.com.registrodeponto.models.Usuario;
import br.com.registrodeponto.models.RegistroDetalhado;
import br.com.registrodeponto.models.RegistroDePonto;
import br.com.registrodeponto.response.Response;
import br.com.registrodeponto.service.RegistroDePontoService;

@RunWith(MockitoJUnitRunner.class)
public class RegistroDePontoControllerTest {

	@InjectMocks
	RegistroDePontoController registroDePontoController;

	@Mock
	RegistroDePontoService registroDePontoService;

	@MockBean
	private BindingResult bindingResult;

	@Test
	public void createTest() {
		Usuario dadosUsuario = new Usuario(1L, "Fernando", 32036550800L, "teste@test.com", LocalDate.now());
		RegistroDePonto dadosBatida = new RegistroDePonto(1L, dadosUsuario, LocalDateTime.now(), 1);
		RegistroDePontoDTO dadosBatidaDTO = new RegistroDePontoDTO(1L, 1);

		bindingResult = Mockito.mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);
		when(registroDePontoService.create(dadosBatidaDTO)).thenReturn(dadosBatida);
		ResponseEntity<Response<String>> dadosRetorno = registroDePontoController.create(dadosBatidaDTO, bindingResult);

		assertEquals(dadosRetorno.getStatusCode(), HttpStatus.OK);

	}

	@Test
	public void createTestError() {
		RegistroDePontoDTO dadosBatidaDTO = new RegistroDePontoDTO(1L, 1);

		bindingResult = Mockito.mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);

		ResponseEntity<Response<String>> dadosRetorno = registroDePontoController.create(dadosBatidaDTO, bindingResult);

		assertEquals(dadosRetorno.getStatusCode(), HttpStatus.BAD_REQUEST);

	}
	
	@Test
	public void listaBatidaTest() {
		List<RegistroDetalhado> dadosBatidas = new ArrayList<RegistroDetalhado>();
		RegistroDetalhado dadosBatida1 = new RegistroDetalhado("entrada",LocalDateTime.now());
		RegistroDetalhado dadosBatida2 = new RegistroDetalhado("saída",LocalDateTime.now());
		dadosBatidas.add(dadosBatida1);
		dadosBatidas.add(dadosBatida2);
		RegistroDetalhadoDTO dadosDetalhado = new RegistroDetalhadoDTO();
		dadosDetalhado.setNomeUsuario("Fernando Martini");
		dadosDetalhado.setBatidas(dadosBatidas);
		dadosDetalhado.setHorasTrabalhadas("13:13:13 horas");
		
		when(registroDePontoService.getBatidasByUsuario(1L)).thenReturn(dadosDetalhado);
		RegistroDetalhadoDTO dadosRetorno = registroDePontoController.listaBatida(1L);

		assertEquals(dadosRetorno.getNomeUsuario(), dadosDetalhado.getNomeUsuario());
	}

}

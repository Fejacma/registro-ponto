package br.com.registrodeponto.service;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.registrodeponto.models.DTOs.RegistroDetalhadoDTO;
import br.com.registrodeponto.models.DTOs.RegistroDePontoDTO;
import br.com.registrodeponto.models.Usuario;
import br.com.registrodeponto.models.RegistroDePonto;
import br.com.registrodeponto.repositories.UsuarioRepository;
import br.com.registrodeponto.repositories.RegistroDePontoRepository;

@RunWith(MockitoJUnitRunner.class)
public class BatidaPontoServiceTest {
	
	@InjectMocks
	RegistroDePontoService batidaService;
	

	@Mock
	RegistroDePontoRepository batidaPontoRepository;
	@Mock
	UsuarioRepository usuarioRepository;
	
	
	@Test
	public void testCreate() {
		Usuario dadosUsuario = new Usuario(1L,"Fernando",35207774875L,"teste@test.com",LocalDate.now());
		
		RegistroDePonto dadosBatida = new RegistroDePonto(1L,dadosUsuario,LocalDateTime.now(),1);
		
		RegistroDePontoDTO dadosBatidaDto = new RegistroDePontoDTO(1L,1);
		when(batidaPontoRepository.save(any(RegistroDePonto.class))).thenReturn(dadosBatida);
		when(usuarioRepository.findById(dadosBatidaDto.getIdUsuario())).thenReturn(Optional.of(dadosUsuario));
		
		RegistroDePonto batidaSalva = batidaService.create(dadosBatidaDto);
		assertTrue(batidaSalva.getIdBatida().equals(dadosBatida.getIdBatida()));
	}
	
	
	@Test
	public void testCreateRetornoNulo() {
						
		RegistroDePontoDTO dadosBatidaDto = new RegistroDePontoDTO(1L,1);
		
		
		RegistroDePonto batidaSalva = batidaService.create(dadosBatidaDto);
		assertNull(batidaSalva);
	}
	
	
	
	public void getBatidasByUsuario() {
		Usuario dadosUsuarioEnviado = Mockito.mock(Usuario.class);;
		dadosUsuarioEnviado.setId(1L);
		Usuario dadosUsuario = new Usuario(1L,"Fernando",35207774875L,"teste@test.com",LocalDate.now());
		List<RegistroDePonto> dadosBatidas = Mockito.mock(List.class);
		
		RegistroDePonto dadosBatida1 = new RegistroDePonto(1L,dadosUsuario,LocalDateTime.now(),1);
		RegistroDePonto dadosBatida2 = new RegistroDePonto(2L,dadosUsuario,LocalDateTime.now(),2);
		dadosBatidas.add(dadosBatida1);
		dadosBatidas.add(dadosBatida2);		
		
		when(batidaPontoRepository.findAllByResponsavel(dadosUsuarioEnviado)).thenReturn(dadosBatidas);
		
		RegistroDetalhadoDTO dadosEncontrados = batidaService.getBatidasByUsuario(dadosUsuario.getId());
		
		assertTrue(dadosUsuario.getNomeCompleto().equalsIgnoreCase(dadosEncontrados.getNomeUsuario()));
	}
	
	@Test
	public void getBatidasByUsuarioNull() {
		Usuario dadosUsuarioEnviado = new Usuario();
		dadosUsuarioEnviado.setId(1L);
		Usuario dadosUsuario = new Usuario(1L,"Fernando",35207774875L,"teste@test.com",LocalDate.now());
		List<RegistroDePonto> dadosBatidas = new ArrayList<RegistroDePonto>();
		
		RegistroDePonto dadosBatida1 = new RegistroDePonto(1L,dadosUsuario,LocalDateTime.now(),1);
		RegistroDePonto dadosBatida2 = new RegistroDePonto(2L,dadosUsuario,LocalDateTime.now(),2);
		dadosBatidas.add(dadosBatida1);
		dadosBatidas.add(dadosBatida2);		
		
		
		
		RegistroDetalhadoDTO dadosEncontrados = batidaService.getBatidasByUsuario(dadosUsuario.getId());
		
		assertNull(dadosEncontrados);
	}

}
